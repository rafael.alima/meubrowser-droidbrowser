package br.com.samples.meubrowser;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText EndUrl;
    private Button BtnIR;
    private Button BtnVol;
    private Button BtnAvan;
    private Button BtnSob;
    private WebView myWebView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EndUrl = (EditText) findViewById(R.id.EndUrl) ;
        BtnIR = (Button) findViewById(R.id.BtnIR);
        BtnVol = (Button) findViewById(R.id.BtnVol);
        BtnAvan = (Button) findViewById(R.id.BtnAvan);

        BtnIR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myWebView.loadUrl("https://"+EndUrl.getText().toString());

            }
        });

        BtnVol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(myWebView.canGoBack()){
                   myWebView.goBack();

               }
            }
        });

        BtnAvan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(myWebView.canGoForward()){
                    myWebView.goForward();
                }
            }
        });


        MyWebViewClient myWebViewClient = new MyWebViewClient();
        myWebView = findViewById(R.id.wv_my_browser);

        myWebView.setWebViewClient(myWebViewClient);
        myWebView.loadUrl("https://www.globo.com");
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
    }

    public void onButtonClick (View v){
        Intent myIntent = new Intent(getBaseContext(),
                sobreActivity.class);
        startActivity(myIntent);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK && myWebView.canGoBack()) {
            myWebView.goBack();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }
}
